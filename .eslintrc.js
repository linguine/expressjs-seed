module.exports = {
    root: true,
    env: {
        node: true,
        es6: true,
    },
    extends: 'airbnb-base',
    rules: {
        'class-methods-use-this': 'off',
        'import/prefer-default-export': 'warn',
        'no-console': 'error',
        'no-debugger': 'error',
        'indent': [ 'error', 4 ],
        'comma-dangle': [ 'error', 'always-multiline' ],
    },
    'overrides': [],
    parserOptions: {
        parser: 'babel-eslint',
    },
    globals: {
        it: 'readonly',
        describe: 'readonly',
        beforeEach: 'readonly',
    }
};
