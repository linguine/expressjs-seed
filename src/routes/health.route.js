export default class Health {
    get(req, res) {
        res.json({ healthy: true });
        res.status(200);
        return res;
    }
}
