import { assert } from 'chai';

import Info from './info.route';

let route;
let response;

describe('Info Route', () => {
    beforeEach(() => {
        route = new Info();
        response = {
            responseStatus: 200,
            responseJson: {},
            // eslint-disable-next-line func-names
            json(json) {
                this.responseJson = json;
            },
            // eslint-disable-next-line func-names
            status(status) {
                this.responseStatus = status;
            },
        };
    });
    describe('get()', () => {
        it('Should return the name of the service', () => {
            route.get({}, response);
            assert.equal('Express JS Seed project', response.responseJson.name);
        });
        it('Should respond with a 200 status', () => {
            route.get({}, response);
            assert.equal(200, response.responseStatus);
        });
    });
});
