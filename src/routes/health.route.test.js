import { assert } from 'chai';

import Health from './health.route';

let route;
let response;

describe('Health Routes', () => {
    beforeEach(() => {
        route = new Health();
        response = {
            responseStatus: 200,
            responseJson: {},
            json(json) {
                this.responseJson = json;
            },
            status(status) {
                this.responseStatus = status;
            },
        };
    });
    describe('get()', () => {
        it('Should return an object containing a `healthy` key', () => {
            route.get({}, response);
            assert.isTrue(response.responseJson.healthy);
        });
        it('Should respond with a 200 status', () => {
            route.get({}, response);
            assert.equal(200, response.responseStatus);
        });
    });
});
