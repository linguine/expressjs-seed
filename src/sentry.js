import * as Sentry from '@sentry/node';

function sentryActive() {
    return Object.prototype.hasOwnProperty.call(process.env, 'SENTRY_DSN');
}

function setupSentry(app) {
    if (sentryActive()) {
        Sentry.init({ dsn: process.env.SENTRY_DSN });
        app.use(Sentry.Handlers.requestHandler());
    } else {
        // eslint-disable-next-line no-console
        console.error('Sentry not initialized. Missing DSN Env value');
    }
}

function addSentryErrorHandler(app) {
    app.use(Sentry.Handlers.errorHandler());
}


export { setupSentry, addSentryErrorHandler };
