import express from 'express';
import {
    Health, Info,
} from './routes';
import { setupSentry, addSentryErrorHandler } from './sentry';


const app = express();
const port = 3000;

setupSentry(app);

const infoRoute = new Info();
// We need to bind the get method into it's controller, so when we use `this`, it refers to itself.
app.use('/', infoRoute.get.bind(infoRoute));

const healthRoute = new Health();
app.use('/health', healthRoute.get.bind(healthRoute));

addSentryErrorHandler(app);

// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
