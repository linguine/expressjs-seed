# ExpressJS ES6 Seed project.

Can be used to save a day of project setup.  

Clone it, change the git origin, and start writing code.  

We prefer Yarn for installation of node modules,   
but this can easily be changed by deleting the yarn.lock file and running `npm install` to generate an npm lock file.

Scripts included:
* `npm run watch` -> Watch files and reload server on changes
* `npm run dev` -> Run dev server
* `npm run build` -> Transpile javascript for production
* `npm run server:prod` -> Start production server
* `npm run lint` -> Lint files
* `npm run test` -> Run unit tests
* `npm run test:ci` -> Run unit tests in CI which ignores any `only` flags. It will tun ALL tests.
* `npm run test:watch` -> Run unit tests in watch mode. File changes with rerun the tests.

##### Installation

Via Docker:
```
docker-compose up -d
```

Local:
```
yarn install
npm run watch
```

Both will serve the application at http://localhost:3000


### Included in project

##### Docker setup

* Dockerfile
* docker-compose file for local development

##### ES6 Setup

* Babel transpilers
* Babel setup scripts

##### CI Setup

* Gitlab CI file to on Gitlab
* Health & Info endpoints for smoke tests & readiness / liveness probes
* Chai & Mocha for unit testing
* Eslint for code linting.

##### Project

* Nodemon for auto reloading
* Sentry for error monitoring
* dotenv integration to load values from .env file. Included you will find a example file.

##### Error monitoring setup

We enjoy using sentry and have included that in this project.

If you would like to use sentry, merely add a `SENTRY_DSN=[YOUR_DSN]` value in `.env` and reload the server.

Sentry will now monitor errors from the project.

If you prefer not to use sentry, remove SENTRY_DSN from .env.example & derivatives,   
remove the following lines from `src/index.js`, delete the `src/sentry.js` file & remove `@sentry/node` from the `package.json` file
```ecmascript 6
import { setupSentry, addSentryErrorHandler } from './sentry';

setupSentry(app);

addSentryErrorHandler(app);
```
