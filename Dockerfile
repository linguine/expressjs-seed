FROM node:11-alpine as build

RUN mkdir -p /code
WORKDIR /code

COPY package.json /code
COPY yarn.lock /code

RUN npm install -g yarn && \
    export PATH=$PATH:/code/node_modules/.bin && \
    yarn install

COPY . /code

RUN npm run clean && \
    npm run build

FROM node:11-alpine as production

RUN mkdir -p /code/dist

WORKDIR /code

COPY --from=build /code/dist ./dist
COPY --from=build /code/package.json .
COPY --from=build /code/yarn.lock .

RUN yarn install --prod

CMD ["npm", "run", "server:prod"]

FROM build as local

# When files change we want to re-compile and restart the server.
# In Alpine the notify tools are missing which emit events used by nodemon to pick up files changes.
# We need to add inotify-tools to add back those events
RUN apk add inotify-tools && yarn install

CMD ["npm", "run", "watch"]

FROM production
